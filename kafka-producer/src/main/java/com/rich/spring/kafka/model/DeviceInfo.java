package com.rich.spring.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceInfo {

    private long id;
    private String name;
    private String type;
//    private double lat;
//    private double long;


}
