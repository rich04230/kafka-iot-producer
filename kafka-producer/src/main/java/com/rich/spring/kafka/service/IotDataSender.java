package com.rich.spring.kafka.service;

import com.rich.spring.kafka.model.SensorLog;
import com.rich.spring.kafka.model.DeviceInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class IotDataSender {

    public static final String TOPIC_DEVICE_TEMPERATURE = "streaming.iot.temperature";
    public static final String TOPIC_DEVICE = "streaming.iot.device";

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Qualifier("kafkaJsonTemplate")
    @Autowired
    KafkaTemplate kafkaTemplate;

    public void send(SensorLog sensorLog) {
        log.info("send device: {} to topic: {}", sensorLog, TOPIC_DEVICE_TEMPERATURE);
        kafkaTemplate.send(TOPIC_DEVICE_TEMPERATURE, Long.toString(sensorLog.getId()), sensorLog);
    }

    public void sendDeviceInfo() {
        DeviceInfo d1 = new DeviceInfo(1, "device1", "motion sensor");
        DeviceInfo d2 = new DeviceInfo(2, "device2", "iot sensor");
        DeviceInfo d3 = new DeviceInfo(3, "device3", "iot sensor");
        DeviceInfo d4 = new DeviceInfo(4, "device4", "mobile sensor");
        DeviceInfo d5 = new DeviceInfo(5, "device5", "mobile sensor");
        DeviceInfo d6 = new DeviceInfo(6, "device6", "mobile sensor");
        DeviceInfo d7 = new DeviceInfo(7, "device7", "iot sensor");
        DeviceInfo d8 = new DeviceInfo(8, "device8", "iot sensor");
        DeviceInfo d9 = new DeviceInfo(9, "device9", "iot sensor");
        DeviceInfo d10 = new DeviceInfo(10, "device10", "iot sensor");

        List<DeviceInfo> devices = Arrays.asList(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10);
        devices.forEach(d-> {
            kafkaTemplate.send(TOPIC_DEVICE, Long.toString(d.getId()), d);
            log.info("Send device: {} info into Kafka", d);
        });

    }
}
