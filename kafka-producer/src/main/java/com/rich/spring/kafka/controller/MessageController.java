package com.rich.spring.kafka.controller;

import com.rich.spring.kafka.model.Message;
import com.rich.spring.kafka.service.MessageListener;
import com.rich.spring.kafka.service.MessageSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    MessageSender messageSender;

    @Autowired
    MessageListener messageListener;

    @PostMapping
    public void sendMessage() {
        messageSender.send("Hello Spring Kafka, timestamp: " + System.currentTimeMillis());
    }

    @GetMapping
    public Message getAllRecords() {
        return new Message(messageListener.getAllRecords());
    }
}
