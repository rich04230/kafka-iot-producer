package com.rich.spring.kafka.controller;

import com.rich.spring.kafka.service.IotDataSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/devices")
public class DeviceController {

    @Autowired
    IotDataSender iotDataSender;

    @PostMapping
    public void initDevice() {
        iotDataSender.sendDeviceInfo();
    }

}
